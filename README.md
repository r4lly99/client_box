## Client Box

Aplikasi buat client (musti berjalan bareng dengan `client_box`)

###Connect 

endpoint : `http://localhost:8080/api/connect`

methode : `POST`

payload : `{
   "from" : "Ani",
   "password" : "123"
 }`
 
 ###Send Current Present 
 
 endpoint : `http://localhost:8080/api/present?to=Ani`
 
 methode : `GET`
 
 response : `{ "lon": 120.84513,"lat": -5.21462 }`
 
 ###Send Request
 
 endpoint : `http://localhost:8080/api/request`
  
 methode : `POST`
 
 payload : ```{
              "from": "Ani",
              "lon" : 120.84513,
              "lat" : -5.21462
            }```
  
 response : `{
                   "successful": true
               }`
               
 ###Accept Request
 
 endpoint : `http://localhost:8080/api/request`
 
 methode : `GET`
 
 response : ```
 {
   "from": "Ani",
   "to": "Joko",
   "lon": 120.84513,
   "lat": -5.21462,
   "status": "accept"
}
               ```
 ###Receive Location
 
 endpoint : `http://localhost:8080/api/receive`
 
 methode : `GET`
 
 response : ```{
                   "to": "Joko",
                   "lon": 120.84513,
                   "lat": -5.21462
               }```
               
               
 

               
               
                   
               
 
 