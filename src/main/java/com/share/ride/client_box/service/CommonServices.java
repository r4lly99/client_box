package com.share.ride.client_box.service;

import com.share.ride.client_box.domain.CurrentPresent;

public interface CommonServices {

    boolean getConnection(String userName, String password) ;

    CurrentPresent sendCurrentPresence(String userName) ;

}
