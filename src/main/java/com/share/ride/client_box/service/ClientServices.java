package com.share.ride.client_box.service;

import com.share.ride.client_box.domain.AcceptRequest;
import com.share.ride.client_box.domain.SendRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ClientServices {

    @POST("api/receive")
    Call<SendRequest> sendRequest(@Body SendRequest request) ;


    @GET("api/accept")
    Call<AcceptRequest> acceptRequest() ;

}
