package com.share.ride.client_box.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendRequest {

    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("lon")
    @Expose
    private double lon;
    @SerializedName("lat")
    @Expose
    private double lat;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
