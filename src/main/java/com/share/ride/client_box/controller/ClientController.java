package com.share.ride.client_box.controller;


import com.google.gson.Gson;
import com.share.ride.client_box.domain.*;
import com.share.ride.client_box.service.ClientServices;
import com.share.ride.client_box.service.CommonServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class ClientController {

    @Autowired
    private ClientServices clientServices;

    @Autowired
    private CommonServices commonServices;

    private Connect connect;
    private AcceptRequest acceptRequest;
    private SendLocation sendLocation;

    private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

    private boolean LOGIN_STATUS = false ;

    @RequestMapping(value = "/request" , method = RequestMethod.POST)
    public ResponseEntity sendRequest(@RequestBody SendRequest request) throws IOException {
        if(LOGIN_STATUS) {
            Gson gson = new Gson();
            logger.info("Sending Request : {}", gson.toJson(request));
            Call<SendRequest> requestCall = clientServices.sendRequest(request);
            Response<SendRequest> response = requestCall.execute();
                if (response.isSuccessful()) {
                    return ResponseEntity.ok(response);
                } else {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.body());
                }
            } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(request);
        }
    }

    @RequestMapping(value = "/post" , method = RequestMethod.POST)
    public ResponseEntity postRequest(@RequestBody AcceptRequest request) throws IOException {
        Gson gson = new Gson();
        logger.info("Get Request Info : {}", gson.toJson(request));
        acceptRequest = request;
        if(acceptRequest.equals(request)){
            return ResponseEntity.ok(acceptRequest);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(acceptRequest);
        }
    }

    @RequestMapping(value = "/request" , method = RequestMethod.GET)
    public ResponseEntity getRequest() throws IOException {
        Gson gson = new Gson();
        logger.info("Request Status : {}", gson.toJson(acceptRequest));
        return ResponseEntity.ok(acceptRequest);
    }

    @RequestMapping(value = "/connect" , method = RequestMethod.POST)
    public ResponseEntity connect(@RequestBody Connect request) throws IOException {
        Gson gson = new Gson();
        logger.info("Login Info : {}", gson.toJson(request));
        LOGIN_STATUS = commonServices.getConnection(request.getFrom(), request.getPassword());
        if(LOGIN_STATUS){
            connect = request ;
            return ResponseEntity.ok("connect success");
        }else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(request);
        }

    }

    @RequestMapping(value = "/present" , method = RequestMethod.GET)
    public ResponseEntity sendCurrentPresent(@RequestParam("from") String from) throws IOException {
        Gson gson = new Gson();
        logger.info("Present Info : {}", gson.toJson(connect));
        if(LOGIN_STATUS && connect.getFrom().contentEquals(from)) {
            CurrentPresent currentPresent = commonServices.sendCurrentPresence(from);
            logger.info("Present Status : {}", gson.toJson(currentPresent));
            return ResponseEntity.ok(currentPresent);
        }else{
            logger.info("Present Info Failed : {}", gson.toJson(from));
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(from);
        }
    }

    @RequestMapping(value = "/send" , method = RequestMethod.POST)
    public ResponseEntity sendLocation(@RequestBody SendLocation request) throws IOException {
        Gson gson = new Gson();
        logger.info("Receive Location Info : {}", gson.toJson(request));
        sendLocation = request;
        if(sendLocation.equals(request)){
            return ResponseEntity.ok(sendLocation);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(sendLocation);
        }
    }

    @RequestMapping(value = "/receive" , method = RequestMethod.GET)
    public ResponseEntity receiveLocation() throws IOException {
        Gson gson = new Gson();
        logger.info("Request Status : {}", gson.toJson(sendLocation));
        return ResponseEntity.ok(sendLocation);
    }

}
