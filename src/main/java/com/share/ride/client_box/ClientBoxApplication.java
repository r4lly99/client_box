package com.share.ride.client_box;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientBoxApplication.class, args);
	}
}
