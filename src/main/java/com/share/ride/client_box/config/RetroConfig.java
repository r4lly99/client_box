package com.share.ride.client_box.config;

import com.share.ride.client_box.service.ClientServices;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class RetroConfig {

    @Bean
    public OkHttpClient client() {
        final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        final OkHttpClient builtClient = okHttpClientBuilder.build();
        return builtClient;
    }

    @Bean
    public Retrofit retrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl("http://localhost:9090")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Bean
    public ClientServices clientServices(Retrofit retrofit){
        return retrofit.create(ClientServices.class);
    }


}
